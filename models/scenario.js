var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Scenario = new Schema({
  title: String,
  description: String,
  extarnalToken: String,
  url: String,
  screenData: [ Schema.Types.Mixed ],
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  createdAt: { type : Date, default: Date.now  },
  lastModified: { type : Date, default: Date.now  },
  hidden: { type: Boolean, default: false }
},{
  timestamps: true
});

module.exports = mongoose.model('Scenario', Scenario);