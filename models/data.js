var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CollectedData = new Schema({
  scenarioId: { type: Schema.Types.ObjectId, ref: 'Scenario' },
  name: String,
  scenarioVersion: String,
  screenData: { type: Schema.Types.Mixed }
},{
  timestamps: true
});

module.exports = mongoose.model('CollectedData', CollectedData);
