var express   = require('express');
var router    = express.Router();
var Scenario  = require('../models/scenario');
var Verify    = require('./verify');
var config    = require('../config');

/**
 * @swagger
 * definition:
 *   Scenario:
 *     type: object
 *     properties:
 *       title:
 *         type: string
 *       description:
 *         type: string
 *       screenData:
 *         type: array
 *         items:
 *           type: object
 *       user:
 *         type: object
 *       createdAt:
 *         type: date
 *       lastModified:
 *         type: date
 *       hidden:
 *         type: boolean
 *   PostScenario:
 *     type: object
 *     properties:
 *       title:
 *         type: string
 *       description:
 *         type: string
 *       screenData:
 *         type: array
 *         items:
 *           type: object
 */

/**
 * @swagger
 * /scenarios/:
 *   get:
 *     tags:
 *       - Scenarios
 *     description: Returns all scenarios
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of scenarios
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Scenario'
 *       500:
 *         description: Error message
 */
router.get('/', function(req, res, next) {
  Scenario.find({}, function (err, scenario) {
    if (err) throw err;
    res.json(scenario);
  })
    .populate('user');
});

/**
 * @swagger
 * /scenarios/getById/{scenarioId}:
 *   get:
 *     tags:
 *       - Scenarios
 *     description: Returns a specific scenario
 *     parameters:
 *       - name: scenarioId
 *         in: path
 *         description: The ID of a scenario
 *         required: true
 *         type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A scenario
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Scenario'
 *       500:
 *         description: Error message
 */
router.get('/getById/:scenarioId', function(req, res, next) {
  Scenario.findById(req.params.scenarioId, function(err, scenario){
    if (err) throw err;
    res.json(scenario);
  })
    .populate('user');
});

/**
 * @swagger
 * /scenarios/getByUserId/{userId}:
 *   get:
 *     tags:
 *       - Scenarios
 *     description: Returns all scenario by a user
 *     parameters:
 *       - name: userId
 *         in: path
 *         description: The ID of a scenario
 *         required: true
 *         type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Scenario'
 *       500:
 *         description: Error message
 */
router.get('/getByUserId/:userId', function(req, res, next) {
  Scenario.find({user: req.params.userId}, function(err, scenarios){
    if (err) throw err;
    res.json(scenarios);
  })
    .populate('user');
});

/**
 * @swagger
 * /scenarios/:
 *   post:
 *     tags:
 *       - Scenarios
 *     description: Creates a new scenario
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Scenario
 *         description: Scenario object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/PostScenario'
 *     responses:
 *       200:
 *         description: Success
 *       403:
 *         description: No token provided
 *       500:
 *         description: Registration error
 *         schema:
 *           type: object
 *           properties:
 *             err:
 *               type: object
 *               properties:
 *                 name:
 *                   type: string
 *                 message:
 *                   type: string
 */
router.post('/', Verify.verifyOrdinaryUser, function(req, res, next) {
  if (req.headers.authorization){
    req.body.extarnalToken = req.headers.authorization;
  }
  req.body.user = req.decoded._doc._id;
  
  
  
  Scenario.create(req.body, function (err, scenario) {
    if (err) throw err;
    var url = config.swagger.definition.host+'/mobile/#/app/scenario/'+scenario._id;
    scenario.url = url;
    scenario.save(function (err, s) {
      if (err) throw err;
      res.json(s);
    });
    //res.json(scenario);
  });
});


/**
 * @swagger
 * /scenarios/{scenarioId}:
 *   put:
 *     tags:
 *       - Scenarios
 *     description: Updates a specific scenario
 *     parameters:
 *       - name: scenarioId
 *         in: path
 *         description: The ID of a sceanrio
 *         required: true
 *         type: string
 *       - name: Scenario
 *         description: Scenario object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/PostScenario'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A scenario
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Scenario'
 *       500:
 *         description: Error message
 */
router.put('/:scenarioId', function(req, res, next) {
  Scenario.findByIdAndUpdate(req.params.scenarioId, {
    $set: req.body,
    $inc: {__v: 1}
  }, {
    new: true
  }, function (err, scenario) {
    if (err) throw err;
    res.json(scenario);
  });
});

/**
 * @swagger
 * /scenarios/{scenarioId}:
 *   delete:
 *     tags:
 *       - Scenarios
 *     description: Deletes a specific scenario
 *     parameters:
 *       - name: scenarioId
 *         in: path
 *         description: The ID of a sceanrio
 *         required: true
 *         type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A scenario
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Scenario'
 *       500:
 *         description: Error message
 */

router.delete('/:scenarioId', Verify.verifyOrdinaryUser, function(req, res, next) {
  Scenario.findById(req.params.scenarioId, function (err, scenario) {
    if (scenario.user != req.decoded._doc._id) {
      var err = new Error('You are not authorized to perform this operation!');
      err.status = 403;
      return next(err);
    }
    scenario.remove(function (err) {
      if (err) throw err;
      res.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      res.end('Deleted scenario with the id: '+scenario._id);
    });
  });
});

module.exports = router;