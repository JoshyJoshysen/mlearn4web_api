var express  = require('express');
var router   = express.Router();
var Data     = require('../models/data');
var Scenario = require('../models/scenario');
var Verify   = require('./verify');

/**
 * @swagger
 * definition:
 *   Data:
 *     type: object
 *     properties:
 *       scenarioId:
 *         type: string
 *       groupname:
 *         type: string
 *       scenarioVersion:
 *         type: string
 *       timestamp:
 *         type: date
 *       data:
 *         type: object
 *   PostData:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       scenarioId:
 *         type: string
 *       screenData:
 *         type: array
 *         items:
 *           type: object
 */

/**
 * @swagger
 * /data/:
 *   get:
 *     tags:
 *       - Data
 *     description: Returns all collected data
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of collected data
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Data'
 *       500:
 *         description: Error message
 */

router.get('/', function(req, res, next) {
  Data.find({}, function (err, scenario) {
    if (err) throw err;
    res.json(scenario);
  })
    .populate('scenarioId')
  ;
});

/**
 * @swagger
 * /data/getDataById/{dataId}:
 *   get:
 *     tags:
 *       - Data
 *     description: Returns data
 *     parameters:
 *       - name: dataId
 *         in: path
 *         description: The ID of a dataset
 *         required: true
 *         type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns the dataset
 *         schema:
 *           $ref: '#/definitions/Data'
 *       500:
 *         description: Error message
 */
router.get('/getDataById/:dataId', function(req, res, next) {
  Data.findById(req.params.dataId, function(err, scenario){
    res.json(scenario);
  })
    //.populate('user')
  ;
});

/**
 * @swagger
 * /data/getDataByScenario/{scenarioId}:
 *   get:
 *     tags:
 *       - Data
 *     description: Returns data of a specific scenario
 *     parameters:
 *       - name: scenarioId
 *         in: path
 *         description: The ID of a scenario
 *         required: true
 *         type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns an array of datasets
 *       500:
 *         description: Error message
 */
router.get('/getDataByScenario/:scenarioId', function(req, res, next) {
  Data.find({scenarioId: req.params.scenarioId}, function(err, data){
    res.json(data);
  })
  //.populate('user')
  ;
});

/**
 * @swagger
 * /data/:
 *   post:
 *     tags:
 *       - Data
 *     description: Creates a new dataset
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Data
 *         description: Data object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/PostData'
 *     responses:
 *       200:
 *         description: Registration Successful!
 *       500:
 *         description: Registration error
 *         schema:
 *           type: object
 *           properties:
 *             err:
 *               type: object
 *               properties:
 *                 name:
 *                   type: string
 *                 message:
 *                   type: string
 */
router.post('/', function(req, res, next) {
  Scenario.findById(req.body.scenarioId, function (e, scenario) {
    if (e) throw e;
    req.body.scenarioVersion = scenario.__v;
    Data.create(req.body, function (err, data) {
      if (err) throw err;
      var id = data._id;
      res.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      res.end('Added the Data with id: ' + id);
    });
  });
});


/**
 * @swagger
 * /data/{dataId}:
 *   put:
 *     tags:
 *       - Data
 *     description: Updates a specific dataset
 *     parameters:
 *       - name: dataId
 *         in: path
 *         description: The ID of a dataset
 *         required: true
 *         type: string
 *       - name: Data
 *         description: Data object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/PostData'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns the dataset
 *         schema:
 *           $ref: '#/definitions/Data'
 *       500:
 *         description: Error message
 */
router.put('/:dataId', function(req, res, next) {
  Data.findByIdAndUpdate(req.params.dataId, {
    $set: req.body
  }, {
    new: true
  }, function (err, data) {
    if (err) throw err;
    res.json(data);
  });
});

/**
 * @swagger
 * /data/{dataId}:
 *   delete:
 *     tags:
 *       - Data
 *     description: Delets a specific dataset
 *     parameters:
 *       - name: dataId
 *         in: path
 *         description: The ID of a dataset
 *         required: true
 *         type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A scenario
 *         schema:
 *           $ref: '#/definitions/Data'
 *       500:
 *         description: Error message
 */
router.delete('/:dataId', Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req, res, next) {
  Data.remove({_id: req.params.userId}, function (err, resp) {
      if (err) throw err;
      res.json(resp);
    }
  );
});

module.exports = router;