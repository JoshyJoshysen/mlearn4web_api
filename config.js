var fs = require('fs');

//if user and password is required uncomment the relevant sections
var db = {
  db: 'mlearn4web'
  ,host: 'localhost'
  ,port: 27017 //20762
  //,username: 'mlearn'
  //,password: 'm1234'
};

var dbUrl = 'mongodb://';
//dbUrl += db.username+':'+db.password+'@';
dbUrl += db.host + ':' + db.port;
dbUrl += '/' + db.db;

exports.db = {
  db: db,
  secret : 'megaubersecret',
  url: dbUrl
};

exports.passport = {
  'secretKey': '23523-12345-67890-23523',
  'facebook': {
    clientID: '163255604109508',
    clientSecret: '14fbe353886cb73fab7bd29e045d13c4',
    callbackURL: 'https://localhost:3333/users/facebook/callback'
  }
};

//SSL
var key = fs.readFileSync('./ssl/private.key');
var cert = fs.readFileSync('./ssl/certificate.pemgit branch -d feature/login');

exports.ssl = {
  key: key,
  cert: cert
};

exports.swagger = {
  definition : {
    info: {
      title: 'mLearn4web API',
      version: '1.0.0',
      description: 'API for mLearn4web. Most operations needs a token for authorization. The token can be submitted in the body (token:{token}), query (?token={token}) or header (x-access-token). Enjoy!'
    },
    host: 'localhost:3333',
    schemes: ['https', 'http'],
    securityDefinitions: {
      /*Bearer: {
        type: 'apiKey',
        description: 'Submits a token to identify a user',
        name: 'token',
        in: 'query'
      },*/
      Bearer: {
        type: 'apiKey',
        description: 'Submits a token to identify a user',
        name: 'x-access-token',
        in: 'header'
      }
    },
    basePath: '/'
  }
};